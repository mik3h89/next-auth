import Amplify from 'aws-amplify'

export function Configure(ampAuthConfig) {
  Amplify.configure(ampAuthConfig)
}
