import React, { createContext, useEffect, useCallback } from 'react'
import { Auth } from 'aws-amplify'

const AuthContext = createContext()

/**
 * This context allows for easy shared auth methods (it may feel redundant)
 * It helps keeps in case we ever change providers
 * we won't need to change auth methods in different places everything is
 * in one place can just update the providers functions
 */

function AuthProvider({ config, ...props }) {
  const currentAuthenticatedUser = useCallback(() => {
    return Auth.currentAuthenticatedUser()
  }, [])

  const signIn = useCallback((usernameOrEmail, password) => {
    if (!usernameOrEmail || !password) return
    return Auth.signIn(usernameOrEmail, password)
  }, [])

  const signOut = useCallback(() => {
    return Auth.signOut()
  }, [])

  const forgotPassword = useCallback((username) => {
    return Auth.forgotPassword(username)
  }, [])

  const completeNewPassword = useCallback(async (newPassword) => {
    const user = await currentAuthenticatedUser()
    return Auth.completeNewPassword(user, newPassword)
  }, [])

  return (
    <AuthContext.Provider
      value={{
        currentAuthenticatedUser,
        signIn,
        signOut,
        forgotPassword,
        completeNewPassword,
      }}
      {...props}
    />
  )
}

const useAuth = () => React.useContext(AuthContext)

export { AuthProvider, useAuth }
