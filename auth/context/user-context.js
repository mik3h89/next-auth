import React, { createContext, useEffect, useState, useCallback } from 'react'
import { useAuth } from './auth-context'

const UserContext = createContext({})

/**
 * Since user profile information is coming from the database
 * and not cognito this provider will keep cognito user data
 * and profile together for ease of use
 */

const UserProvider = (props) => {
  const [user, setUser] = useState(null)
  const { currentAuthenticatedUser } = useAuth()

  console.log(user)

  const setCurrentUser = useCallback(async () => {
    try {
      const user = await currentAuthenticatedUser()
      const { jwtToken } = user.signInUserSession.idToken
      const headers = {
        'Content-Type': 'application/json',
        Authorization: jwtToken,
      }
      let profile = {
        Data: {
          FirstName: 'Mike',
          LastName: 'Hughes',
        },
      }
      // Real endpoint currently broken - Working with Bryan to fix
      // let profile = await fetch(
      //   // `https://0z49an3q26.execute-api.us-east-1.amazonaws.com/dev/goService-dev-customer-admin?uuid=${uuid}`,
      //   `https://0pow3wegm8.execute-api.us-east-1.amazonaws.com/get/user`,
      //   {
      //     method: 'GET',
      //     mode: 'cors',
      //     headers,
      //     cache: 'no-cache',
      //   }
      // )
      // profile = await profile.json()
      console.log(profile)
      setUser({ attributes: user.attributes, profile: profile.Data })
    } catch (err) {
      console.log(err)
      setUser(null)
    }
  }, [setUser])

  useEffect(() => {
    setCurrentUser()
  }, [setCurrentUser])

  // Don't render component until user data actually exists
  return !!user ? <UserContext.Provider value={user} {...props} /> : <></>
}

const useUser = () => React.useContext(UserContext)

export { UserProvider, useUser }

//
// const { jwtToken } = req.user.signInUserSession.idToken
// const uuid = req.user.attributes.sub
// const headers = {
//   'Content-Type': 'application/json',
//   Authorization: jwtToken,
// }
// let profile = await fetch(
//   `https://0z49an3q26.execute-api.us-east-1.amazonaws.com/dev/goService-dev-customer-admin?uuid=${uuid}`,
//   {
//     method: 'GET',
//     mode: 'cors',
//     headers,
//     cache: 'no-cache',
//   }
// )
// profile = await profile.json()
// res.json({ attributes: req.user.attributes, profile: profile.Data })
