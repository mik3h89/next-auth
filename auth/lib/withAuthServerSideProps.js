// this file is a wrapper with defaults to be used in both API routes and `getServerSideProps` functions
import { withSSRContext } from 'aws-amplify'

export function withAuthServerSideProps(handler) {
  return async (ctx) => {
    const { Auth } = withSSRContext(ctx)

    try {
      let user = await Auth.currentAuthenticatedUser()
      // TODO: Next JS serialize issue
      user = JSON.parse(JSON.stringify(user))
      console.log('HAS USER')
      if (handler && typeof handler === 'function') {
        return await handler({ ...ctx, user })
      } else {
        return {
          props: {
            user,
          },
        }
      }
    } catch (err) {
      ctx.res.setHeader('location', '/signin')
      ctx.res.statusCode = 302
      ctx.res.end()
    }

    return {
      props: {},
    }
  }
}
