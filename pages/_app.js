import '../styles/globals.css'
import { AuthProvider, UserProvider } from '../auth/context'
import config from '../amplify/config'
import { Configure } from '../auth'

/**
 * Configure needs to live outside the React app
 * due to using Amplify with server side logic
 */
Configure(config)

function MyApp({ Component, pageProps, router }) {
  return (
    <AuthProvider>
      {!!router.pathname.startsWith('/signin') ? (
        <Component {...pageProps} />
      ) : (
        <UserProvider>
          <Component {...pageProps} />
        </UserProvider>
      )}
    </AuthProvider>
  )
}

export default MyApp
