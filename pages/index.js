import { useAuth, useUser } from '../auth/context'
import { withAuthServerSideProps } from '../auth'
import { useRouter } from 'next/router'

export default function Home() {
  const { signOut } = useAuth()
  const user = useUser()
  const router = useRouter()

  const handleSignOut = async () => {
    await signOut()
    router.push('/signin')
  }

  return (
    <div>
      Hello {user.profile.FirstName}
      <button onClick={handleSignOut}>Sign Out</button>
    </div>
  )
}

export const getServerSideProps = withAuthServerSideProps()
