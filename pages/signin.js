import { useState } from 'react'
import { useAuth } from '../auth/context'
import { useRouter } from 'next/router'

export default function SignInPage() {
  const [formState, setFormState] = useState({
    email: '',
    password: '',
  })
  const { signIn } = useAuth()
  const router = useRouter()

  const onChange = (e) => {
    const { name, value } = e.target
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }))
  }

  const onSubmit = async (e) => {
    try {
      e.preventDefault()
      const { email, password } = formState
      await signIn(email, password)
      router.push('/')
    } catch (err) {
      console.log('Error:', err)
    }
  }

  return (
    <form onSubmit={onSubmit}>
      <input name="email" value={formState.email} onChange={onChange} />
      <input
        name="password"
        type="password"
        value={formState.password}
        onChange={onChange}
      />
      <button type="submit">Submit</button>
    </form>
  )
}
